$(document).ready(function () {

    $('.container').on('click', '.video', function(e) {
        let video_id = e.currentTarget.id;
        let id = video_id.slice(9, video_id.length + 1);
        $('#loading_' + id).removeClass('hide');

        if(id !== '') {
            $.ajax({
                type: "POST",
                url: "site/show",
                data: {
                    videoId: id
                },
                success: function (response) {
                    if(response === 'Error 1') {
                        console.log('Что-то пошло не так при загрузке данных в БД.');
                    } else {
                        if($('#big_video_id_' + id).length <= 0) {
                            $('#video_id_' + id).after(function() {
                            return '<div id = "big_video_id_' + id + '" class = "show-video">' + response + '</div>';
                        });
                        }
                        $('#loading_' + id).addClass('hide');
                        $('#video_id_' + id).hide();
                    }
                },
                error: function () {
                    alert(' Перейдите на страницу http://youtubeapi');
                    console.log('Что-пошло не так при передаче данных.');
                },
            });
        }
    });

    $('.container').on('click', '.close-video-img', function(e) {
        let img_id = e.currentTarget.id;

        $('#video_id_' + img_id).show();
        $('#big_video_id_' + img_id).remove();
    });
});
