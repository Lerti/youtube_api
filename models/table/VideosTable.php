<?php

namespace app\models\table;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "videos".
 *
 * @property int $id
 * @property string $video_id
 * @property int $views
 */
class VideosTable extends ActiveRecord
{
    public static function tableName()
    {
        return 'videos';
    }

    public function rules()
    {
        return [
            [['views'], 'integer'],
            [['video_id'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'video_id' => 'Video ID',
            'views' => 'Views',
        ];
    }

    /**
     * function find video from table with selected video_id
     * @param $id string
     * @return static|null
    */
    public function findByVideoId($id)
    {
        return static::findOne(['video_id' => $id]);
    }

    /**
     * function add video to table
     * @param $id string
     * @return bool
     */
    public function addVideo($id)
    {
        $this->video_id = $id;
        $this->views = 1;
        if($this->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * function add view to video in table
     * @param $id string
     * @return bool
     */
    public function addVideoView($id)
    {
        $model = VideosTable::findByVideoId($id);
        $model->views ++;

        if($model->save()) {
            return true;
        } else {
            return false;
        }
    }
}
