<?php

namespace app\models;

use Google_Client;

class GoogleAccount
{
    protected $client;
    protected $developerKey = 'AIzaSyAp9HQ8Br9Axt5lVPGK5GarPNr_-LoSny8';
    // действительный = 'AIzaSyAp9HQ8Br9Axt5lVPGK5GarPNr_-LoSny8';
    // не работает = 'AIzaSyDOQbGXBZd7cofG2bLCocXoAkpFX3GvtHc';


    public function addAccount(Google_Client $client) {
        $client->setDeveloperKey($this->developerKey);
        $client->setScopes(['https://www.googleapis.com/auth/youtube',]);
        $client->setAccessType('offline');
    }
}