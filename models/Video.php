<?php
namespace app\models;

class Video
{
    const MAX_NUMBER = 10;

    public function generateVideoNumber()
    {
        $maxNumber = self::MAX_NUMBER;
        $minNumber = 1;
        $arrValues[0] = ['Выберите количество видео'];

        while ($minNumber <= $maxNumber) {
            $arrValues[$minNumber] = $minNumber;
            $minNumber += 1;
        }
        return $arrValues;
    }
}