<?php
/**
 * Created by PhpStorm.
 * User: 805621
 * Date: 09.11.2019
 * Time: 4:24
 */

namespace app\models;

use yii\base\Model;

class Search extends Model {

    public $name;
    public $number;

    public function attributeLabels() {
        return [
            'name' => '',
            'number' => '',
        ];
    }

    public function rules() {
        return [
            ['name', 'trim'],
            ['name', 'required', 'message' => 'Введите название видео для поиска'],
            ['number', 'number', 'min' => 1, 'message' => 'Выберете количество видео для поиска'],
        ];
    }

}