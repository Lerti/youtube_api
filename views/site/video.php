<? /**
 * @var $videos array
 */
?>

<ul class = "list-video">
	<? if (!is_null($videos)) {
	    foreach($videos as $video) {
	        $id = $video['id']['videoId'];
            $img = $video['snippet']['thumbnails']['medium']['url'];
            $alt = $video['snippet']['title'];
            $title = $video['snippet']['title'];

            if(strlen($alt) > 32) {
                $title = substr($title, 0, 32) . '...';
            }
	        ?>
            <div>
                <li class = "video" id = "video_id_<?= $id ?>">
                    <img src = "<?= $img ?>" alt = "<?= $alt ?>" />
                    <div id = "loading_<?= $id ?>" class = "hide loading-image">
                        <img src = "img/loading.webp" />
                    </div>
                    <h4><?= $title ?></h4>
                </li>
            </div>

        <?}
	} ?>
</ul>