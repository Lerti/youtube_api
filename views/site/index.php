<?php

/**
 * @var $this yii\web\View
 * @var $selectors array
 * @var $videos array
 */

use yii\helpers\Html;
use app\models\Video;
use yii\widgets\ActiveForm;

$this->title = 'Поиск видео';
$maxSelectors = Video::MAX_NUMBER;
?>
<div>

    <? $form = ActiveForm::begin(['id' => 'feedback', 'class' => 'form-horizontal']); ?>
    <?= $form->field($model, 'name')->textInput(['class' => 'form-control',
        'placeholder' => 'Вставьте сюда название видео для поиска']); ?>
    <?= $form->field($model, 'number')->dropDownList($selectors, [
        'id' => 'posts-number',
        'text' => 'Выберите количество видео',
        'class' => 'form-control',
    ]); ?>
    <div class = 'source-buttons'>
        <?= Html::submitButton('Поиск видео',
            ['class' => 'btn-get-video', 'id' => 'find-video']) ?>
    </div>
    <?php ActiveForm::end(); ?>


    <?= $this->render('video', [
        'videos' => $videos,
    ]) ?>
</div>
