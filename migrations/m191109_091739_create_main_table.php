<?php

use yii\db\Migration;

/**
 * Handles the creation of table `videos`.
 */
class m191109_091739_create_main_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('videos', [
            'id' => $this->primaryKey(),
            'video_id' => $this->string(),
            'views' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('videos');
    }
}
