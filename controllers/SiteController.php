<?php

namespace app\controllers;


use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Video;
use app\models\Search;
use app\models\GoogleAccount;
use Google_Client;
use Google_Service_YouTube;
use app\models\table\VideosTable;
//use Google_Service_Analytics;
use yii;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $client = new Google_Client;
        $accountGoogle = new GoogleAccount();
        $accountGoogle->addAccount($client);
        $youtube = new Google_Service_YouTube($client);

        $video = new Video();
        $videos = null;
        $selectors = $video->generateVideoNumber();

        $model = new Search();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $videoName = $model->name;
                $videoNumber = $model->number;
                $videos = $youtube->search->listSearch('id, snippet', array(
                    'q' => $videoName,
                    'type' => 'video',
                    'maxResults' => $videoNumber));
            }
        }

        return $this->render('index', [
            'videos' => $videos,
            'selectors' => $selectors,
            'model' => $model]);
    }

    /**
     * Displays video-page.
     *
     * @return string
     */
    public function actionShow()
    {
        if (Yii::$app->request->isAjax) {

            $id = Yii::$app->request->post('videoId');

            $videos = new VideosTable();
            if($videos->findByVideoId($id)) {
                if(!$videos->addVideoView($id)) {
                    return 'Error 1';
                }
            } else {
                if(!$videos->addVideo($id)) {
                    return 'Error 1';
                }
            }

            return $this->renderAjax('show', [
                'videoId' => $id]);
        }
    }
}
